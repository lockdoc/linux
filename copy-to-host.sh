#!/bin/bash
if [ ${#} -lt 2 ];
then
	exit 42
fi

scp vmlinux ${1}:${2}/vmlinux-5-4-`date +%Y%m%d``cat .scmversion`${3}
