#if !defined(__LOCKDOC_H__)
#define __LOCKDOC_H__

#if defined(X86_BOOT_CODE) && defined(CONFIG_LOCKDOC)
#undef CONFIG_LOCKDOC
#endif

#ifdef CONFIG_LOCKDOC
#include <linux/kernel.h>
#include <linux/irqflags.h>
#include <linux/spinlock.h>
#include <linux/seqlock_types.h>
#include <linux/semaphore.h>
#include <linux/lockdoc_event.h>

#define DELIMITER	"#"
#define DELIMITER_CHAR	'#'
#define PING_CHAR	'p'
#define MK_STRING(x)	#x
#define IO_PORT_LOG	(0x00e9)

#define log_lock(_acquire,_ptr,_file,_line)	{			\
	spinlock_t *__spinlock;										\
	raw_spinlock_t *__rawspinlock;									\
	rwlock_t *__rwlock;										\
	struct mutex *__mutex;										\
	seqlock_t *__seqlock;										\
	const seqlock_t *__constseqlock;										\
	seqcount_t *__seqcount;									\
	const seqcount_t *__constseqcount;									\
	struct semaphore *__semaphore;									\
	struct rw_semaphore *__rwsemaphore;								\
	unsigned long *__bitspinlock;									\
	if (_ptr == (void*)PSEUDOLOCK_ADDR_RCU) {									\
		__log_lock(_acquire,_ptr,_file,_line,PSEUDOLOCK_NAME_RCU,0);	\
	} else if (_ptr == (void*)PSEUDOLOCK_ADDR_HARDIRQ) {									\
		__log_lock(_acquire,_ptr,_file,_line,PSEUDOLOCK_NAME_HARDIRQ,0);	\
	} else if (_ptr == (void*)PSEUDOLOCK_ADDR_SOFTIRQ) {									\
		__log_lock(_acquire,_ptr,_file,_line,PSEUDOLOCK_NAME_SOFTIRQ,0);	\
	} else if (__same_type(_ptr,__spinlock)) {							\
		__log_lock(_acquire,_ptr,_file,_line,"spinlock_t",0);	\
	} else if (__same_type(_ptr,__rwlock)) {							\
		__log_lock(_acquire,_ptr,_file,_line,"rwlock_t",0);	\
	} else if (__same_type(_ptr,__mutex)) {								\
		__log_lock(_acquire,_ptr,_file,_line,"mutex",0);	\
	} else if (__same_type(_ptr,__seqlock) || __same_type(_ptr,__constseqlock)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "seqlock_t",0);	\
	} else if (__same_type(_ptr,__seqcount) || __same_type(_ptr,__constseqcount)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "seqcount_t",0);	\
	} else if (__same_type(_ptr,__rawspinlock)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "raw_spinlock_t",0);	\
	} else if (__same_type(_ptr,__semaphore)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "semaphore",0);	\
	} else if (__same_type(_ptr,__rwsemaphore)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "rw_semaphore",0);	\
	} else if (__same_type(_ptr,__bitspinlock)) {							\
		__log_lock(_acquire,_ptr,_file,_line, "bit_spin_lock",0);	\
	} else{												\
		__log_lock(_acquire,_ptr,_file,_line, "unknown",0);	\
	}												\
}

extern struct log_action la_buffer;

/* Basic port I/O */
static inline void outb_(u8 v, u16 port)
{
	asm volatile("outb %0,%1" : : "a" (v), "dN" (port));
}

int32_t lockdoc_get_ctx(void);

static inline void log_memory(int alloc, const char *datatype, const void *ptr, size_t size) {
	unsigned long flags;
	
	raw_local_irq_save(flags);

	memset(&la_buffer,0,sizeof(la_buffer));

	la_buffer.action = (alloc == 1 ? LOCKDOC_ALLOC : LOCKDOC_FREE);
	la_buffer.ctx = lockdoc_get_ctx();
	la_buffer.ptr = (unsigned long)ptr;
	la_buffer.size = size;
	/*
	 * One could use a more safe string function, e.g., strlcpy. 
	 * However, we want these *log* functions to be fast.
	 * We therefore skip all sanity checks, and all that stuff.
	 * To ensure any string buffer contains a valid string, we
	 * always write a NULL byte at its end.
	 */
	strncpy(la_buffer.type,datatype,LOG_CHAR_BUFFER_LEN);
	la_buffer.type[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	outb_(PING_CHAR,IO_PORT_LOG);

	raw_local_irq_restore(flags);
}

/*
 * The parameter irqsync should actually be an enum IRQ_SYNC. To avoid circular dependencies in include/linux/{irqflags,bottom_half}.h which contains
 * a declaration of this function, this parameter has to be an int.
 */
static inline void __log_lock(int lock_op, const void *ptr, const char *file, int line, const char *lock_type, int flags) {
	unsigned long eflags;

	raw_local_irq_save(eflags);

	memset(&la_buffer,0,sizeof(la_buffer));

	la_buffer.action = LOCKDOC_LOCK_OP;
	la_buffer.ctx = lockdoc_get_ctx();
	la_buffer.lock_op = lock_op;
	la_buffer.ptr = (unsigned long)ptr;
	strncpy(la_buffer.type,lock_type,LOG_CHAR_BUFFER_LEN);
	la_buffer.type[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	strncpy(la_buffer.file,file,LOG_CHAR_BUFFER_LEN);
	la_buffer.file[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	strncpy(la_buffer.lock_member, "empty",LOG_CHAR_BUFFER_LEN);
	la_buffer.line = line;
	la_buffer.flags = flags;
	outb_(PING_CHAR,IO_PORT_LOG);

	raw_local_irq_restore(eflags);
}

void lockdoc_send_current_task_addr(void);
void lockdoc_send_preempt_count_addr(void);
void lockdoc_send_pid_offset(void);
void lockdoc_send_kernel_version(void);
#else
#define MK_STRING(x)	#x
#define log_lock(_acquire,_ptr,_file,_line)
#define log_memory(alloc, datatype, ptr, size)
#define lockdoc_send_current_task_addr()
#define lockdoc_send_preempt_count_addr()
#define lockdoc_send_pid_offset()
#define lockdoc_send_kernel_version()
#endif /* !CONFIG_LOCKDOC */

#endif // __LOCKDOC_H__
